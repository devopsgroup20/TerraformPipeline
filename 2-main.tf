resource "aws_s3_bucket" "example" {
  bucket = "mybucketfortestinggitlab1"

  tags = {
    Name        = "mybucketfortestinggitlab1"
    Environment = "Dev"
  }
}